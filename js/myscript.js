var mainCtrl = function($scope,$http){
	$scope.tasks =[
		{'body':'do this 1','done':false},
		{'body':'do this 2','done':false},
		{'body':'do this 3','done':true},
		{'body':'do this 4','done':false},
	];

	$scope.addNew = function(){
		//$scope.tasksに連想配列を追加する。
		//$scope.newTaskBodyにてデータを取得:get
		$scope.tasks.push({"body":$scope.newTaskBody,"done":false});
		//$scope.newTaskBodyの値を空にする:set
		$scope.newTaskBody = '';

	}

	$scope.getDoneCount = function(){
		var count = 0;
			//angular.forEachでスコープを回せる
			angular.forEach($scope.tasks,function(task){
				//task.doneがtrueだったら1,falseだったら0を加算
				count += task.done ? 1 : 0;
			});

		return count;
	}

	$scope.deleteDone = function(){
		//現在のtasksを一旦避難
		var oldTasks = $scope.tasks;
		//tasksを初期化
		$scope.tasks = [];
		//避難させたtasks:oldTasksを回していく
		angular.forEach(oldTasks,function(task){
			//oldTasksのdoneがfalseだったら(未処理だったら)、
			//$scopeのモデルtaskに追加
			if(!task.done) $scope.tasks.push(task);
		});
	}
}